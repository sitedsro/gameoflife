const width = 600
const height = 600
const rows = 30
const cols = 30
const cellWidth = width / rows
const cellHeight = height / cols
const cells = new Array(rows)
function setup() {

  createCanvas(width, height)
  background(0)

  class Cell {
    constructor(i, j){
      this.life = Math.random()
      this.drawCell(i, j)
    }
    checkLife(numI, numJ) {
      let neighbours = 0
      //for(let i = (numI - 1); i <= (numI + 1); i++){
      //  for(let j = (numJ - 1); j <= (numJ + 1); j++) {
      //    if(i !== numI && j !== numJ && j > 0 && i > 0 && i <= rows && j <= cols && cells[i][j] && cells[i][j].life > 0.5) {
      //     neighbours++
      //    }
      //  }
      // }
      let a = numI - 1
      let b = numI + 1
      let c = numJ - 1
      let d = numJ + 1

      if (a < 0) a++
      if (b > rows) b--
      if (c < 0) c++
      if (d > cols) d--

      for (let i = a; i <= b; i++) {
        for (let j = c; j <= d; j++) {
          if (i == numI && j == numJ) continue
          if (cells[i][j].life > 0.5) neighbours++
        }
        console.log(neighbours)
        this.checkNeighbours(neighbours)
      }
    }
    checkNeighbours(neighbours){
      if(neighbours < 2) {
        this.life = 0
        this.drawCell(this.i, this.j)
        console.log("neigh < 2")
      }
    }
    drawCell(i ,j){
      if(this.life > 0.5) fill(255)
      else fill(0)
      rect(i * cellWidth, j * cellHeight, cellWidth, cellHeight)
    }
  }
  
  for (let i = 0; i < cells.length; i++) {
    cells[i] = new Array(cols)
  }
  for(let i = 0; i < cols; i++) {
    for(let j = 0; j < rows; j++) {
      cells[i][j] = new Cell(i, j)

    }
  }
} 
function draw() {
  for(let i = 0; i < cols; i++) {
    for(let j = 0; j < rows; j++) {
      cells[3][3].checkLife(3, 3)
    }
  }
}